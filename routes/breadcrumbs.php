<?php

use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

/**
 * Admin
 */
Breadcrumbs::for('dashboard', function(BreadcrumbTrail $breadcrumbs) {
    $breadcrumbs->push('Админ Панель', route('dashboard'));
});



/**
 * Admin / Students
 */
Breadcrumbs::for('students.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Ученики', route('students.index'));
});

/**
 * Admin / Students / Create Student
 */
Breadcrumbs::for('students.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('students.index');
    $breadcrumbs->push('Добавить Ученика', route('students.create'));
});

/**
 * Admin / Students / [Student Name]
 */
Breadcrumbs::for('students.show', function(BreadcrumbTrail $breadcrumbs, $student)
{
    $breadcrumbs->parent('students.index');
    $breadcrumbs->push($student->first_name, route('students.show', $student->id));
});

/**
 * Admin / Students / [Student Name] / Edit Student
 */
Breadcrumbs::for('students.edit', function(BreadcrumbTrail $breadcrumbs, $student)
{
    $breadcrumbs->parent('students.show', $student);
    $breadcrumbs->push('Редактировать Ученика', route('students.edit', $student->id));
});

/**
 * Admin / Students / Student Name] / Parents
 */
Breadcrumbs::for('students.parents', function(BreadcrumbTrail $breadcrumbs, $student)
{
    $breadcrumbs->parent('students.show', $student);
    $breadcrumbs->push('Родители Ученика', route('students.parents', $student->id));
});


/**
 * Admin / Parents
 */
Breadcrumbs::for('parents.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Родители', route('parents.index'));
});

/**
 * Admin / Parents / Create Parents
 */
Breadcrumbs::for('parents.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('parents.index');
    $breadcrumbs->push('Добавить Родителя', route('parents.create'));
});

/**
 * Admin / Parents / [Parents Name]
 */
Breadcrumbs::for('parents.show', function(BreadcrumbTrail $breadcrumbs, $parent)
{
    $breadcrumbs->parent('parents.index');
    $breadcrumbs->push($parent->first_name, route('parents.show', $parent->id));
});

/**
 * Admin / Parents / [Parent Name] / Edit Parent
 */
Breadcrumbs::for('parents.edit', function(BreadcrumbTrail $breadcrumbs, $parent)
{
    $breadcrumbs->parent('parents.show', $parent);
    $breadcrumbs->push('Редактировать Родителя', route('parents.edit', $parent->id));
});


/**
 * Admin / Confidants
 */
Breadcrumbs::for('confidants.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Доверенные Лица', route('confidants.index'));
});

/**
 * Admin / Confidants / Create Confidants
 */
Breadcrumbs::for('confidants.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('confidants.index');
    $breadcrumbs->push('Добавить Доверенное Лицо', route('confidants.create'));
});

/**
 * Admin / Confidants / [Confidants Name]
 */
Breadcrumbs::for('confidants.show', function(BreadcrumbTrail $breadcrumbs, $confidant)
{
    $breadcrumbs->parent('confidants.index');
    $breadcrumbs->push($confidant->first_name, route('confidants.show', $confidant->id));
});

/**
 * Admin / Confidants / [Confidant Name] / Edit Confidant
 */
Breadcrumbs::for('confidants.edit', function(BreadcrumbTrail $breadcrumbs, $confidant)
{
    $breadcrumbs->parent('confidants.show', $confidant);
    $breadcrumbs->push('Редактировать Доверенное Лицо', route('confidants.edit', $confidant->id));
});


/**
 * Admin / Events
 */
Breadcrumbs::for('events.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('События', route('events.index'));
});

/**
 * Admin / Events / Create Event
 */
Breadcrumbs::for('events.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('events.index');
    $breadcrumbs->push('Добавить Событие', route('events.create'));
});

/**
 * Admin / Events / [Event Name]
 */
Breadcrumbs::for('events.show', function(BreadcrumbTrail $breadcrumbs, $event)
{
    $breadcrumbs->parent('events.index');
    $breadcrumbs->push($event->name, route('events.show', $event->id));
});

/**
 * Admin / Events / [Event Name] / Edit Event
 */
Breadcrumbs::for('events.edit', function(BreadcrumbTrail $breadcrumbs, $event)
{
    $breadcrumbs->parent('events.show', $event);
    $breadcrumbs->push('Редактировать Событие', route('events.edit', $event->id));
});

/**
 * Admin / Events / Event Name] / Logs
 */
Breadcrumbs::for('events.logs', function(BreadcrumbTrail $breadcrumbs, $event)
{
    $breadcrumbs->parent('events.show', $event);
    $breadcrumbs->push('Лог Посещений', route('events.logs', $event->id));
});
