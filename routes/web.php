<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ParentsController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ConfidantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::resource('students', StudentController::class);

    Route::resource('parents', ParentsController::class);

    Route::resource('confidants', ConfidantController::class);

    Route::resource('events', EventController::class);

    Route::get('/students/{student}/parents', [StudentController::class, 'parents'])->name('students.parents');

    Route::get('/events/{event}/logs', [EventController::class, 'logs'])->name('events.logs');
});

require __DIR__.'/auth.php';
