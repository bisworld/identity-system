$(document).ready(function () {
    /*
     |--------------------------------------------------------------------------
     | MENU
     |--------------------------------------------------------------------------
     |
     | Раздвигающееся меню
     |
     */
    let caret = $('.caret');

    caret.parent('a').on('click', function () {
        $(this).parent('li').toggleClass('open');
        $(this).next('ul').slideToggle(50);
    });

    if (caret.parent().parent('li').hasClass('active')) {
        caret.parent().parent('li').addClass('open');
    }

    /*
     |--------------------------------------------------------------------------
     | DROP DOWN
     |--------------------------------------------------------------------------
     |
     | Выпадающее меню
     |
     */
    $('.drop-down-toggle').click(function (e) {
        e.stopPropagation();

        let menu = $(this).next('.drop-down-menu');
        $('.drop-down-menu').not(menu).fadeOut(50);

        (menu.is(':visible'))
            ? menu.fadeOut(50)
            : menu.fadeIn(200);
    });

    $(document).click(function () {
        $('.drop-down-menu').fadeOut(50);
    });

    /*
     |--------------------------------------------------------------------------
     | SELECT
     |--------------------------------------------------------------------------
     |
     | Красивый select
     |
     */
    $('.select').select();

    /*
     |--------------------------------------------------------------------------
     | CSRF TOKEN
     |--------------------------------------------------------------------------
     |
     | Добавляем token для ajax запросов
     |
     */
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _token
        },
        cache: false
    });

    /*
     |--------------------------------------------------------------------------
     | DELETE
     |--------------------------------------------------------------------------
     |
     | Запрос на удаление
     |
     */
    $('[data-delete]').on('click', function () {
        if (!confirm('Вы действительно хотите удалить данный пункт?'))
            return;

        let link = $(this);
        let wLoc = window.location;

        $.ajax({
            url: wLoc.origin + wLoc.pathname + '/' + link.data('delete') + wLoc.search,
            type: "POST",
            dataType: 'JSON',
            data: {
                '_method': 'DELETE'
            }
        }).then(function (response) {
            link.closest('tr').slideUp(500, function () {
                $(this).remove();
            });

            $('body').message({
                status: response.status,
                message: response.message
            });
        }).fail(function (response, status, message) {
            $('body').message({
                status: status,
                message: message
            });
        });
    });
});
