@if ($breadcrumbs)
    <ul class="row">
        @if(request()->is('admin*'))
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Caviar Store</a></li>
        @endif
        @foreach ($breadcrumbs as $breadcrumb)
            @if ( ! $loop->last)
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="active"><span>{{ $breadcrumb->title }}</span></li>
            @endif
        @endforeach
    </ul>
@endif
