@foreach($items as $item)
    <li @lm_attrs($item) @lm_endattrs>
        @if($item->link) <a @if($item->hasChildren())@endif href="{{ $item->url() }}">
            {!! $item->title !!}
            @if($item->hasChildren()) <b class="caret"></b>@endif
        </a>
        @else
            {{$item->title}}
        @endif
        @if($item->hasChildren())
            <ul class="list-none">
                @include('vendor.menu.admin', ['items' => $item->children()])
            </ul>
        @endif
    </li>
    @if($item->divider)
        <li{{ HTML::attributes($item->divider) }}></li>
    @endif
@endforeach


