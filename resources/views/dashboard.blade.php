@extends('layouts.admin')

@section('title', 'Главная страница')
@section('section', 'Админ Панель')
@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Главная</strong> страница</h2>
        </header>

        <article class="body-tile">
            <p>Здесь будет общая информация о системе и статистика</p>
        </article>
    </section>
@endsection
