@extends('layouts.admin')

@section('title', 'Редактирование Ученика')
@section('section', 'Управление Учениками')
@section('breadcrumbs', Breadcrumbs::render('students.edit', $student))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Пользователя</h2>
        </header>

        {!! Form::model($student, ['route' => ['students.update', $student->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('students.form', ['student' => $student])
        {!! Form::close() !!}
    </section>
@stop
