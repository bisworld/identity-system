@extends('layouts.admin')

@section('title', 'Просмотр Ученика')
@section('section', 'Управление Учениками')
@section('breadcrumbs', Breadcrumbs::render('students.show', $student))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Ученика</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $student->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $student->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $student->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $student->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $student->phone }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Рождения:</span>
                    <span class="inp">@datetime($student->created_at)</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $student->status_text }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
@stop
