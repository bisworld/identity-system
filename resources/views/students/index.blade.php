@extends('layouts.admin')

@section('title', 'Обзор Учеников')
@section('section', 'Управление Учениками')
@section('breadcrumbs', Breadcrumbs::render('students.index'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Учеников</h2>

            <nav class="controls">
                <a href="{!! route('students.create') !!}"><i class="fa fa-plus"></i> Добавить Ученика</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>Телефон</th>
                <th>Telegram</th>
                <th class="center">Статус</th>
                <th>Дата Регистрации</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($students as $student)
                <tr>
                    <td>{{ $student->id }}.</td>
                    <td>{{ $student->first_name }}</td>
                    <td>{{ $student->last_name }}</td>
                    <td>{{ $student->middle_name }}</td>
                    <td>{{ $student->phone }}</td>
                    <td>{{ $student->telegram }}</td>
                    <td class="center">{!! $student->status_icon !!}</td>
                    <td>@datetime($student->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('students.parents', [$student->id]) !!}" class="button with-icon"
                           data-tooltip="Родители"><i class="fa fa-user"></i></a>
                        <a href="{!! route('students.show', [$student->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('students.edit', [$student->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $student->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Ученики не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $students->links() }}
    </section>
@stop
