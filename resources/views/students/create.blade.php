@extends('layouts.admin')

@section('title', 'Добавление Ученика')
@section('section', 'Управление Учениками')
@section('breadcrumbs', Breadcrumbs::render('students.create'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Ученика</h2>
        </header>

        {!! Form::open(['route' => 'students.store', 'class' => 'form body-tile']) !!}
        @include('students.form', ['student' => $student])
        {!! Form::close() !!}
    </section>
@stop
