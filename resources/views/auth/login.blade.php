<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Login</title>
    {!! Html::favicon('favicon.ico') !!}
    {!! Html::style('css/reset.css') !!}
    {!! Html::style('css/grid.css') !!}
    {!! Html::style('css/font.css') !!}
    {!! Html::style('css/font-awesome.css') !!}
    {!! Html::style('css/message.css') !!}
    {!! Html::style('css/login.css') !!}
</head>
<body>

<section class="page login">
    <header class="logo">
        <h3 class="text-light text-white text-center">Safety <span class="text-light-red">School</span></h3>
    </header>
    <section class="box">
        {!! Form::open(['route' => 'login', 'class' => 'form']) !!}
            <header class="header">
                <h2 class="text-light text-green-sea text-center">Log In</h2>
            </header>
            <div class="form-item">
                {!! Form::tel('phone', NULL, ['class' => 'inp', 'placeholder' => 'Phone', 'required']) !!}
            </div>
            <div class="form-item">
                {!! Form::password('password', ['class' => 'inp', 'placeholder' => 'Password', 'required']) !!}
            </div>
            <div class="form-item checkboxes-inline">
                {!! Form::checkbox('remember', true, NULL, ['id' => 'remember']) !!}
                {!! Form::label('remember', 'Remember me') !!}
            </div>

            <footer class="footer bg-slate-gray row between">
                {!! Form::reset('Clear', ['class' => 'button light-red upper']) !!}
                {!! Form::submit('Login', ['class' => 'button success upper']) !!}
            </footer>
        {!! Form::close() !!}
    </section>

    <div style="width: 420px; margin: 20px auto 0">
        @include('vendor.message.default')
    </div>
</section>

</body>
</html>
