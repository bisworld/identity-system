@extends('layouts.admin')

@section('title', 'Редактирование События')
@section('section', 'Управление Событиями')
@section('breadcrumbs', Breadcrumbs::render('events.edit', $event))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> События</h2>
        </header>

        {!! Form::model($event, ['route' => ['events.update', $event->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('events.form', ['event' => $event])
        {!! Form::close() !!}
    </section>
@stop
