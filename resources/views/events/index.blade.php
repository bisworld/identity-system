@extends('layouts.admin')

@section('title', 'Обзор События')
@section('section', 'Управление Событиями')
@section('breadcrumbs', Breadcrumbs::render('events.index'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Событий</h2>

            <nav class="controls">
                <a href="{!! route('events.create') !!}"><i class="fa fa-plus"></i> Добавить Событие</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Дата Начала</th>
                <th>Дата Окончания</th>
                <th class="center">Статус</th>
                <th>Дата Создания</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($events as $event)
                <tr>
                    <td>{{ $event->id }}.</td>
                    <td>{{ $event->name }}</td>
                    <td>@datetime($event->start)</td>
                    <td>@datetime($event->finish)</td>
                    <td class="center">{!! $event->status_icon !!}</td>
                    <td>@datetime($event->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('events.logs', [$event->id]) !!}" class="button with-icon"
                           data-tooltip="Лог"><i class="fa fa-tasks"></i></a>
                        <a href="{!! route('events.show', [$event->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('events.edit', [$event->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $event->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">
                        <div class="message warning" role="alert">События не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $events->links() }}
    </section>
@stop
