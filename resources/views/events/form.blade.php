<div class="row">
    <div class="col form-item">
        {!! Form::label('name', 'Название:') !!}
        {!! Form::text('name', null, ['class' => 'inp', 'placeholder' => 'Родительское Собрание', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('start', 'Дата Начала:') !!}
        {!! Form::text('start', null, ['class' => 'inp', 'placeholder' => '10.10.2010 12:12', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('finish', 'Дата Окончания:') !!}
        {!! Form::text('finish', null, ['class' => 'inp', 'placeholder' => '10.10.2010 12:12']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('lat_top', 'Широта (верх):') !!}
        {!! Form::text('lat_top', null, ['class' => 'inp', 'placeholder' => '23.234', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('lon_top', 'Долгота (верх):') !!}
        {!! Form::tel('lon_top', null, ['class' => 'inp', 'placeholder' => '23.234', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('lat_bottom', 'Широта (низ):') !!}
        {!! Form::text('lat_bottom', null, ['class' => 'inp', 'placeholder' => '23.234', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('lon_bottom', 'Долгота (низ):') !!}
        {!! Form::tel('lon_bottom', null, ['class' => 'inp', 'placeholder' => '23.234', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('status', 'Статус:') !!}
        {!! Form::select('status', $event->statusList(), null, ['class' => 'select', 'required']) !!}
    </div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>
