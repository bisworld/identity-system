@extends('layouts.admin')

@section('title', 'Обзор Лога Посещений')
@section('section', 'Управление Логом Посещений')
@section('breadcrumbs', Breadcrumbs::render('events.logs', $event))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Лога Посещений</h2>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Тип Роли</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th class="center">Статус</th>
                <th>Дата Запроса</th>
                <th>Дата Подтверждения</th>
            </tr>
            </thead>
            <tbody>
            @forelse($logs as $log)
                <tr>
                    <td>{{ $log->id }}.</td>
                    <td>{{ $log->owner }}</td>
                    <td>{{ $log->parent->first_name ?? '' }}</td>
                    <td>{{ $log->parent->last_name ?? '' }}</td>
                    <td>{{ $log->parent->middle_name ?? '' }}</td>
                    <td class="center">{!! $log->status_icon !!}</td>
                    <td>@datetime($log->created_at)</td>
                    <td>@datetime($log->updated_at)</td>
                </tr>
            @empty
                <tr>
                    <td colspan="8">
                        <div class="message warning" role="alert">Посещения не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $logs->links() }}
    </section>
@stop
