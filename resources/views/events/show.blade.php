@extends('layouts.admin')

@section('title', 'Просмотр События')
@section('section', 'Управление Событиями')
@section('breadcrumbs', Breadcrumbs::render('events.show', $event))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> События</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $event->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $event->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $event->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $event->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $event->phone }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Рождения:</span>
                    <span class="inp">@datetime($event->created_at)</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $event->status_text }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
@stop
