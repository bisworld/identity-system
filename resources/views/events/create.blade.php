@extends('layouts.admin')

@section('title', 'Добавление События')
@section('section', 'Управление Событиями')
@section('breadcrumbs', Breadcrumbs::render('events.create'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> События</h2>
        </header>

        {!! Form::open(['route' => 'events.store', 'class' => 'form body-tile']) !!}
        @include('events.form', ['event' => $event])
        {!! Form::close() !!}
    </section>
@stop
