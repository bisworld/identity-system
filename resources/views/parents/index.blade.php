@extends('layouts.admin')

@section('title', 'Обзор Родителей')
@section('section', 'Управление Родителями')
@section('breadcrumbs', Breadcrumbs::render('parents.index'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Родителей</h2>

            <nav class="controls">
                <a href="{!! route('parents.create') !!}"><i class="fa fa-plus"></i> Добавить Родителя</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>Телефон</th>
                <th>Telegram</th>
                <th class="center">Статус</th>
                <th>Дата Регистрации</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($parents as $parent)
                <tr>
                    <td>{{ $parent->id }}.</td>
                    <td>{{ $parent->first_name }}</td>
                    <td>{{ $parent->last_name }}</td>
                    <td>{{ $parent->middle_name }}</td>
                    <td>{{ $parent->phone }}</td>
                    <td>{{ $parent->telegram }}</td>
                    <td class="center">{!! $parent->status_icon !!}</td>
                    <td>@datetime($parent->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('parents.show', [$parent->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('parents.edit', [$parent->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $parent->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Родители не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $parents->links() }}
    </section>
@stop
