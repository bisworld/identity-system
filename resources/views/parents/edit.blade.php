@extends('layouts.admin')

@section('title', 'Редактирование Родителя')
@section('section', 'Управление Родителями')
@section('breadcrumbs', Breadcrumbs::render('parents.edit', $parent))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Родителя</h2>
        </header>

        {!! Form::model($parent, ['route' => ['parents.update', $parent->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('parents.form', ['parent' => $parent])
        {!! Form::close() !!}
    </section>
@stop
