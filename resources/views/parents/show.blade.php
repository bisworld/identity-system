@extends('layouts.admin')

@section('title', 'Просмотр Родителя')
@section('section', 'Управление Родителями')
@section('breadcrumbs', Breadcrumbs::render('parents.show', $parent))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Родителя</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $parent->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $parent->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $parent->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $parent->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $parent->phone }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Рождения:</span>
                    <span class="inp">@datetime($parent->created_at)</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Ученик:</span>
                    <span class="inp">{{ $parent->student->full_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $parent->status_text }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
@stop
