@extends('layouts.admin')

@section('title', 'Добавление Родителя')
@section('section', 'Управление Родителями')
@section('breadcrumbs', Breadcrumbs::render('parents.create'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Родителя</h2>
        </header>

        {!! Form::open(['route' => 'parents.store', 'class' => 'form body-tile']) !!}
        @include('parents.form', ['parent' => $parent])
        {!! Form::close() !!}
    </section>
@stop
