@extends('layouts.admin')

@section('title', 'Просмотр Доверенного Лица')
@section('section', 'Управление Доверенными Лицами')
@section('breadcrumbs', Breadcrumbs::render('confidants.show', $confidant))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Доверенного Лица</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $confidant->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $confidant->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $confidant->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $confidant->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $confidant->phone }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Рождения:</span>
                    <span class="inp">@datetime($confidant->created_at)</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Ученик:</span>
                    <span class="inp">{{ $confidant->student->full_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $confidant->status_text }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
@stop
