@extends('layouts.admin')

@section('title', 'Обзор Доверенных Лиц')
@section('section', 'Управление Доверенными Лицами')
@section('breadcrumbs', Breadcrumbs::render('confidants.index'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Доверенных Лиц</h2>

            <nav class="controls">
                <a href="{!! route('confidants.create') !!}"><i class="fa fa-plus"></i> Добавить Доверенное Лицо</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>Телефон</th>
                <th>Telegram</th>
                <th class="center">Статус</th>
                <th>Дата Регистрации</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($confidants as $confidant)
                <tr>
                    <td>{{ $confidant->id }}.</td>
                    <td>{{ $confidant->first_name }}</td>
                    <td>{{ $confidant->last_name }}</td>
                    <td>{{ $confidant->middle_name }}</td>
                    <td>{{ $confidant->phone }}</td>
                    <td>{{ $confidant->telegram }}</td>
                    <td class="center">{!! $confidant->status_icon !!}</td>
                    <td>@datetime($confidant->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('confidants.show', [$confidant->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('confidants.edit', [$confidant->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $confidant->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Доверенные Лица не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $confidants->links() }}
    </section>
@stop
