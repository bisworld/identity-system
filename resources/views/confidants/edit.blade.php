@extends('layouts.admin')

@section('title', 'Редактирование Доверенного Лица')
@section('section', 'Управление Доверенными Лицами')
@section('breadcrumbs', Breadcrumbs::render('confidants.edit', $confidant))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Доверенного Лица</h2>
        </header>

        {!! Form::model($confidant, ['route' => ['confidants.update', $confidant->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('confidants.form', ['confidant' => $confidant])
        {!! Form::close() !!}
    </section>
@stop
