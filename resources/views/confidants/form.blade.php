<div class="row">
    <div class="col form-item">
        {!! Form::label('last_name', 'Фамилия:') !!}
        {!! Form::text('last_name', null, ['class' => 'inp', 'placeholder' => 'Иванов', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('first_name', 'Имя:') !!}
        {!! Form::text('first_name', null, ['class' => 'inp', 'placeholder' => 'Иван', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('middle_name', 'Отчество:') !!}
        {!! Form::text('middle_name', null, ['class' => 'inp', 'placeholder' => 'Иванович']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('telegram', 'Telegram:') !!}
        {!! Form::text('telegram', null, ['class' => 'inp', 'placeholder' => '@ivan', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('phone', 'Телефон:') !!}
        {!! Form::tel('phone', null, ['class' => 'inp', 'placeholder' => '79001234567', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('birthday', 'Дата Рождения:') !!}
        {!! Form::date('birthday', null, ['class' => 'inp', 'placeholder' => '12.12.2010', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('student_id', 'Ученик:') !!}
        {!! Form::select('student_id', \App\Models\Student::all()->pluck('full_name', 'id'), null, ['class' => 'select', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('status', 'Статус:') !!}
        {!! Form::select('status', $confidant->statusList(), null, ['class' => 'select', 'required']) !!}
    </div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>
