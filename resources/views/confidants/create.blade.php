@extends('layouts.admin')

@section('title', 'Добавление Доверенного Лица')
@section('section', 'Управление Доверенными Лицами')
@section('breadcrumbs', Breadcrumbs::render('confidants.create'))
@section('content')
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Доверенного Лица</h2>
        </header>

        {!! Form::open(['route' => 'confidants.store', 'class' => 'form body-tile']) !!}
        @include('confidants.form', ['confidant' => $confidant])
        {!! Form::close() !!}
    </section>
@stop
