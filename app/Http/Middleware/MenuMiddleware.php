<?php

namespace App\Http\Middleware;

use Menu;
use Closure;
use Lavary\Menu\Builder;
use Illuminate\Http\Request;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('adminMenu', function ($menu) {
            /** @var $menu Builder */
            $menu->add('Админ Панель', ['route' => 'dashboard'])
                 ->prepend('<i class="fa fa-dashboard"></i> ');

            $menu->add('Ученики', ['route' => 'students.index'])
                 ->prepend('<i class="fa fa-users"></i> ')
                 ->active('students/*');

            $menu->add('Родители', ['route' => 'parents.index'])
                 ->prepend('<i class="fa fa-user"></i> ')
                 ->active('parents/*');

            $menu->add('Доверенные лица', ['route' => 'confidants.index'])
                ->prepend('<i class="fa fa-user"></i> ')
                ->active('confidants/*');

            $menu->add('События', ['route' => 'events.index'])
                ->prepend('<i class="fa fa-tasks"></i> ')
                ->active('events/*');
        });

        return $next($request);
    }
}
