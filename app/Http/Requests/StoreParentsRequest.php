<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreParentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'student_id'  => ['required', 'integer', 'exists:students,id'],
            'first_name'  => ['required', 'string'],
            'last_name'   => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'phone'       => ['required', 'unique:parents,phone', 'digits_between:5,20'],
            'telegram'    => ['required', 'string'],
            'birthday'    => ['required', 'date'],
            'status'      => ['required', 'integer'],
        ];
    }
}
