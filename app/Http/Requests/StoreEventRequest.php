<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'       => ['required', 'string'],
            'start'      => ['required', 'string', 'date'],
            'finish'     => ['required', 'string', 'date'],
            'lat_top'    => ['required', 'string'],
            'lon_top'    => ['required', 'string'],
            'lat_bottom' => ['required', 'string'],
            'lon_bottom' => ['required', 'string'],
            'status'     => ['required', 'integer']
        ];
    }
}
