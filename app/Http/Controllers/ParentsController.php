<?php

namespace App\Http\Controllers;

use App\Models\Parents;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Http\Requests\StoreParentsRequest;
use App\Http\Requests\UpdateParentsRequest;

class ParentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View|Factory
     */
    public function index()
    {
        return view('parents.index', [
            'parents' => Parents::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View|Factory
     */
    public function create()
    {
        return view('parents.create',[
            'parent' => new Parents()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreParentsRequest $request
     * @return RedirectResponse
     */
    public function store(StoreParentsRequest $request)
    {
        Parents::create($request->validated());

        return redirect()->route('parents.index')->with('success', 'Родитель Добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param Parents $parent
     * @return View|Factory
     */
    public function show(Parents $parent)
    {
        return view('parents.show', [
            'parent' => $parent
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Parents $parent
     * @return View|Factory
     */
    public function edit(Parents $parent)
    {
        return view('parents.edit', [
            'parent' => $parent
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateParentsRequest $request
     * @param Parents              $parent
     * @return RedirectResponse
     */
    public function update(UpdateParentsRequest $request, Parents $parent)
    {
        $parent->update($request->validated());

        return redirect()->route('parents.index')->with('success', 'Родитель Обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Parents $parent
     * @return JsonResponse
     */
    public function destroy(Parents $parent)
    {
        $parent->delete();

        return response()->json(['status' => 'success', 'message' => 'Родитель Удален!']);
    }
}
