<?php

namespace App\Http\Controllers\Api;

use App\Models\Event;
use App\Models\Log;
use App\Models\Parents;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLogRequest;
use App\Http\Requests\UpdateLogRequest;

class LogController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLogRequest $request
     * @return JsonResponse
     */
    public function store(StoreLogRequest $request)
    {
        Log::create($request->validated() + ['event_id' => Event::first()->id]);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLogRequest $request
     * @return JsonResponse
     */
    public function check(UpdateLogRequest $request)
    {
        $parent = Parents::where(['telegram' => $request->get('parent')])->first();
        $log = Log::where(['status' => 0])->firstOrFail();

        $log->update($request->validated() + ['owner' => 'parent', 'owner_id' => $parent->id, 'status' => 1]);

        return response()->json([
            'success' => true
        ]);
    }
}
