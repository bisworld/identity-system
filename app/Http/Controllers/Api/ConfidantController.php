<?php

namespace App\Http\Controllers\Api;

use App\Models\Parents;
use App\Models\Confidant;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreConfidantRequest;

class ConfidantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(
            Confidant::all()->toArray()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreConfidantRequest $request
     * @return JsonResponse
     */
    public function store(StoreConfidantRequest $request)
    {
        $parent = Parents::where(['telegram' => $request->get('parent')])->first();

        Confidant::create($request->validated() + ['student_id' => $parent->student_id]);

        return response()->json([
            'success' => true
        ]);
    }
}
