<?php

namespace App\Http\Controllers;

use App\Models\Confidant;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Http\Requests\StoreConfidantRequest;
use App\Http\Requests\UpdateConfidantRequest;

class ConfidantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View|Factory
     */
    public function index()
    {
        return view('confidants.index', [
            'confidants' => Confidant::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View|Factory
     */
    public function create()
    {
        return view('confidants.create',[
            'confidant' => new Confidant()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreConfidantRequest $request
     * @return RedirectResponse
     */
    public function store(StoreConfidantRequest $request)
    {
        Confidant::create($request->validated());

        return redirect()->route('confidants.index')->with('success', 'Доверенное Лицо Добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param Confidant $confidant
     * @return View|Factory
     */
    public function show(Confidant $confidant)
    {
        return view('confidants.show', [
            'confidant' => $confidant
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Confidant $confidant
     * @return View|Factory
     */
    public function edit(Confidant $confidant)
    {
        return view('confidants.edit', [
            'confidant' => $confidant
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateConfidantRequest $request
     * @param Confidant              $confidant
     * @return RedirectResponse
     */
    public function update(UpdateConfidantRequest $request, Confidant $confidant)
    {
        $confidant->update($request->validated());

        return redirect()->route('confidants.index')->with('success', 'Доверенное Лицо Обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Confidant $confidant
     * @return JsonResponse
     */
    public function destroy(Confidant $confidant)
    {
        $confidant->delete();

        return response()->json(['status' => 'success', 'message' => 'Доверенное Лицо Удалено!']);
    }
}
