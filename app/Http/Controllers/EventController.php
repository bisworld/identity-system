<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View|Factory
     */
    public function index()
    {
        return view('events.index', [
            'events' => Event::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View|Factory
     */
    public function create()
    {
        return view('events.create',[
            'event' => new Event()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventRequest $request
     * @return RedirectResponse
     */
    public function store(StoreEventRequest $request)
    {
        Event::create($request->validated());

        return redirect()->route('events.index')->with('success', 'Событие Добавлено!');
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return View|Factory
     */
    public function show(Event $event)
    {
        return view('events.show', [
            'event' => $event
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Event $event
     * @return View|Factory
     */
    public function edit(Event $event)
    {
        return view('events.edit', [
            'event' => $event
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventRequest $request
     * @param event              $event
     * @return RedirectResponse
     */
    public function update(UpdateEventRequest $request, event $event)
    {
        $event->update($request->validated());

        return redirect()->route('events.index')->with('success', 'Событие Обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return JsonResponse
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return response()->json(['status' => 'success', 'message' => 'Событие Удалено!']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return View|Factory
     */
    public function logs(Event $event)
    {
        return view('events.logs', [
            'event' => $event,
            'logs' => $event->logs()->paginate()
        ]);
    }
}
