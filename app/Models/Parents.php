<?php

namespace App\Models;

use App\Traits\Statusable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Parents extends Model
{
    use HasFactory, Statusable;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'student_id', 'first_name', 'last_name', 'middle_name', 'phone', 'telegram', 'birthday', 'status'
    ];

    /**
     * Связь родителя и ученика
     *
     * @return BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
