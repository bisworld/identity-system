<?php

namespace App\Models;

use App\Traits\Statusable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Log extends Model
{
    use HasFactory, Statusable;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'event_id', 'lat', 'lon', 'owner', 'owner_id', 'status'
    ];

    /**
     * Связь родителя и ученика
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Parents::class, 'owner_id');
    }
}
