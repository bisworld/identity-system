<?php

namespace App\Models;

use App\Traits\Statusable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    use HasFactory, Statusable;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name', 'start', 'finish', 'lat_top', 'lon_top', 'lat_bottom', 'lon_bottom', 'status'
    ];

    /**
     * Связь события и лога
     *
     * @return HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }
}
