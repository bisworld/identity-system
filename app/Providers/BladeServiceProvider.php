<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->datetime();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Форматирование даты
     *
     * @return void
     */
    private function datetime()
    {
        Blade::directive('datetime', function($expression) {
            $arguments = $this->getArguments($expression);

            switch (count($arguments)) {
                case 1:
                    $date   = current($arguments);
                    $format = "'d.m.Y H:i'";
                    break;

                case 2:
                    list($date, $format) = $arguments;
                    break;

                default:
                    return "Error";
            }

            return "<?php echo ! empty({$date}) ? (new DateTime($date))->format({$format}) : 'Не задано'; ?>";
        });
    }

    /**
     * Получение аргументов переданных в директиву
     *
     * @param $expression
     * @return array
     */
    private function getArguments($expression)
    {
        return array_map(function ($value) {
            return trim($value);
        }, explode(',', trim($expression, '()')));
    }
}
